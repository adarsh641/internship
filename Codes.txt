	//  TERRAFORM  //

--- Download Terraform

* wget https://releases.hashicorp.com/terraform/1.3.7/terraform_1.3.7_linux_amd64.zip

--- Unzip file

* unzip terraform_1.3.7_linux_amd64.zip

--- move binary to a location on our PATH

* sudo mv terraform /usr/local/bin/

--- Verify if Terraform is installed properly

* terraform -v

--- Create a terraform executable file

* main.tf

* code of file  :-

terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

provider "digitalocean" {
  token = "xyz"
}

resource "digitalocean_droplet" "web" {
  image  = "ubuntu-18-04-x64"
  name   = "web-1"
  region = "nyc3"
  size   = "s-1vcpu-1gb"
  ssh_keys = ["abc"]
}

--- Generate a token in digitalocean API and place the key in above token

--- Initilize terraform

* Terraform init

--- Generate ssh key and place in main.tf

* ssh-keygen
 
--- Run and droplet will be created

* Terraform apply



	//  ANSIBLE  //


--- Ansible install

* apt-add-repository -y ppa:ansible/ansible

* apt-get install -y ansible

* apt install python-pip -y


--- Create a playbook file to create 2 users 

* gg.yaml

* code of gg.yml//

- name: Create 2 users with different privileges
  hosts: localhost
  become: yes
  tasks:
    - name: Create user with sudo privileges
      user:
        name: sudo_user
        password: "{{ 'password' | password_hash('sha512', 'avengers') }}"
        shell: /bin/bash
        groups: sudo
        append: yes

    - name: Create normal user
      user:
        name: normal_user
        password: "{{ 'password' | password_hash('sha512', 'flyhigh') }}"
        shell: /bin/bash
        groups:
        append: yes

--- Run using ansible

* ansible-playbook -i "localhost," -c local users.yaml

--- Check the users 

* getent group | less


	
		//  DOCKER  //


--- Update package index

* sudo apt-get update

--- Install packages to allow apt to use repository over HTTPS

* sudo apt-get install apt-transport-https ca-certificates curl software-properties-common

--- Add Docker's GPG Key

* curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

--- Setup the Docker repository

* sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

--- update Packages again

--- Install latest version of Docker CE

* sudo apt-get install docker-ce

--- Start the Docker 

* sudo systemctl start docker

--- Verify that docker if it's installed correctly

* sudo docker run hello-world	