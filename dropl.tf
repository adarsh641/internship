terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

provider "digitalocean" {
  token = var.do_api_key
}

resource "digitalocean_droplet" "web" {
  image  = "ubuntu-18-04-x64"
  name   = "new"
  region = "nyc3"
  size   = "s-1vcpu-1gb"
  ssh_keys = ["cc:b9:0d:3d:d3:ef:0d:7a:a5:a8:16:7c:70:04:fc:3c"]
}
variable "do_api_key" {
  type        = string
  description = "DigitalOcean API Key"
}
~       